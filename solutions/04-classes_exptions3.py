
class ValidatorRunner:
    def __init__(self, serving, validators):
        self.validation_errors = []
        for v in validators:
            try:
                v.validate(serving)
            except ValidationError as err:
                self.validation_errors.append(str(err))
        
        if len(self.validation_errors)>0:
            print("Errors in the serving list:")
            print("-"*30)
            for e in self.validation_errors:
                print(e)

        
black_val_1 = BlackValidator('RUBY')
black_val_2 = BlackValidator('AMELIA')
black_val_3 = BlackValidator('RABBACK')

late_val_1 = LatteValidator("LILY")
late_val_2 = LatteValidator("GRACE")
late_val_3 = LatteValidator("SNORT")

validators_list = [black_val_1, black_val_2, black_val_3, late_val_1, late_val_2, late_val_3]
print()
vr = ValidatorRunner(serving, validators_list)

#vr.validation_errors