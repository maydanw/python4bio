p_val = 0.001
sig_threshold=0.05

res_str = "Results are NOT significant"
if p_val<sig_threshold:
    res_str = "Results are significant"

print(res_str)