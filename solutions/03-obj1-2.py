class Dish:
    def __init__(self,cells, thresholds):
        self.cells = cells
        self.thresholds = thresholds
        
    def __str__(self):
        s = f"Thresholds: {self.thresholds}\n"
        for c in self.cells:
            s = s + "Cell -> " + str(c) + "\n"
        return s    
    
dish = Dish([c1, c2, c3], [50, 150])
print(dish)    