import numpy as np

def shift_sample(sample, new_low, new_high):
    s_min = sample.min()
    s_max = sample.max()
    norm_s = (sample-s_min)/(s_max-s_min)
    return norm_s*(new_high-new_low)+new_low  
    

p = np.zeros([3,8,12])
p[0,:,:] = ((np.random.beta(6,2, [8,12]) * 10) ** 2) * np.pi
p[1,:,:] = shift_sample(np.random.normal(0,1,[8,12]),0, 255)
p[2,:,:] = p[0,:,:]*0.1 + np.random.uniform(-3,4,[8,12])
print(p)
