
from enum import Enum
import random
random.seed(555)
from pprint import pprint

class CoffeeTypes(Enum):
    BLACK_COFFEE=1
    LATTE=2
    DECAF = 3  
    ESPRESSO = 4
    
names_list = ['RUBY', 'EMILY', 'GRACE', 'JESSICA', 'CHLOE', 'SOPHIE', 'LILY', 'AMELIA']

serving = {}
for name in names_list:
    serving[name] = random.choice(list(CoffeeTypes))

print("Serving List")
print("-"*30)
pprint(serving)