import numpy as np
arr = np.random.normal(0,1,[1,1000])
# i=0
factor = 0.8
while True:
    mask = arr>=arr.max()*factor
    arr_mask = arr[mask]
    print(f"Factor: {factor}, Max: {arr.max()}, values:{arr_mask}")
    msk_sz = arr_mask.shape[0]
    if msk_sz<=1:
        break
    arr[mask] = np.random.normal(0,1,msk_sz)
    factor =factor+0.05
    # This I add for safety 
#     i = i+1
#     if i>50:
#         break