def calc_mean_intensity(cells):
    total = 0
    count = 0
    for cell in cells:
        if not is_outlier(cell):
            total = total + cell["total_intensity"]
            count = count + 1
    if count == 0:
        return None
    return total/count

# calc_mean_intensity(cells)