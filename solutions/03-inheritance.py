class Sample:
    def __init__(self, population=[]):
        self.population = np.array(population)
        self.outliers_index = []
        self.find_outliers()
        
    def load_from_file(self, filename):
        self.population = np.genfromtxt(filename, delimiter=",")
        self.find_outliers()
        
    def __repr__(self):
        return f"Sample size: {len(self.population)}, Median: {self.population.median():.3f}"        
        
    def __str__(self):
        return self.__repr__()      
    
    def find_outliers(self):
        if len(self.population) == 0 :
            return 
        median = self.population.median()
        q1 = np.percentile(self.population, 25)
        q3 = np.percentile(self.population, 75)
        irq = q3-q1
        outliers = []
        for index in range(len(self.population)):
            if self.population[index]>q3+1.5*irq or self.population[index]<q1-1.5*irq:
                outliers.append(index)
        self.outliers_index = np.array(outliers)
        

class NormalSample:
    
    def __repr__(self):
        return f"Sample size: {len(self.population)}, Mean: {self.population.mean()}, St.d: {self.population.std():.3f}"
        
    def find_outliers(self):
        if len(self.population) == 0 :
            return 
        mean = self.population.mean()
        std = self.population.std()
        
        # There is a way to solve it without for loop and yet for now this is the correct solution
        outliers = []
        for index in range(len(self.population)):
            if self.population[index]>mean+3*std or self.population[index]<mean-3*std:
                outliers.append(index)
        self.outliers_index = np.array(outliers)