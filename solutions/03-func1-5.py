def is_outlier(cell, limits=[100, 1000]):
    r = cell["radius"]
    if r>limits[1] or r<limits[0]:
        return True
    return False

def calc_mean_intensity(cells, limits=[100, 1000]):
    total = 0
    count = 0
    for cell in cells:
        if not is_outlier(cell, limits):
            total = total + cell["total_intensity"]
            count = count + 1
    if count == 0:
        return None
    return total/count

# calc_mean_intensity(cells, limits=[0,1000])