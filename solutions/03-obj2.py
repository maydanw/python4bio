# %load solutions\\03-obj2.py
import numpy as np

class NormalSample:
    def __init__(self, population=[]):
        self.population = np.array(population)
        self.outliers_index = []
        self.find_outliers()
        
    def load_from_file(self, filename):
        self.population = np.genfromtxt(filename, delimiter=",")
        self.find_outliers()
        
    def __repr__(self):
        return f"Sample size: {len(self.population)}, Mean: {self.population.mean()}, St.d: {self.population.std():.3f}"
    
    def __str__(self):
        self.__repr__()  
        
    def find_outliers(self):
        if len(self.population) == 0 :
            return []
        mean = self.population.mean()
        std = self.population.std()
        
        # There is a way to solve it without for loop and yet for now this is the correct solution
        outliers = []
        for index in range(len(self.population)):
            if self.population[index]>mean+3*std or self.population[index]<mean-3*std:
                outliers.append(index)
        self.outliers_index = np.array(outliers)


# Let's generate the data and save it        
pop_init = np.random.normal(0, 2, 1000)
np.savetxt("./Resources/normal.csv", pop_init, delimiter=",")

s1 = NormalSample([10,20,30, 40, 50])
s2 = NormalSample()
s2.load_from_file("./Resources/normal.csv")
# s2 = Sample().load_from_file("./Resources/normal.csv") # For the advance: This will not work without a simple fix. Can you fix it?
s2