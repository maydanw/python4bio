lst = [1,2,3,2,5,6,77,5,43,9,63]

# Option 1
unq = []
i = 0
while i<len(lst):
    if lst[i] not in unq:
        unq.append(lst[i])
    i = i + 1
print(unq)

# Option 2 (for the smart-aleck)
s = set(lst)
print(s)

# Homework read about python sets and play with it at: https://www.programiz.com/python-programming/set