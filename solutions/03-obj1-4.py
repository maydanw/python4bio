class Dish:
    def __init__(self,cells, thresholds):
        self.cells = cells
        self.thresholds = thresholds
        
    def __str__(self):
        s = f"Thresholds: {self.thresholds}\n"
        for c in self.cells:
            s = s + "Cell -> " + str(c) + "\n"
        return s 
    
    def calc_mean(self):
        # enter your function here
        return len(self.cells) 
    
    def add_cell(self, dict_data):
        c = Cell(dict_data["width"], dict_data["height"], dict_data["total_intensity"])
        self.cells.append(c)

        
c1 = Cell(50,100, 50*100*20)
c2 = Cell(30,30, 250)
c3 = Cell(100,300, 12345)

dish = Dish([c1, c2, c3], [50, 150])
print(dish)
print(dish.calc_mean())

dish.add_cell({"width": 1000, "height": 10, "total_intensity":300000})
print("-"*50)
print(dish)
print(dish.calc_mean())
