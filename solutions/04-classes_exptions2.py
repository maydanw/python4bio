
class ValidaorBase:
    def validate(serving):
        raise NotImplementedError("validate method must be implemented")

# ValidaorBase.validate(serving)   

class ValidationError(Exception):
    pass
    

class BlackValidator(ValidaorBase):
    def __init__(self, name):
        self.name = name
        
    def validate(self, serving):
        if self.name in serving and serving[self.name] != CoffeeTypes.BLACK_COFFEE:
            raise ValidationError(f"{self.name} must have balck coffee")
                
        
    
# black_val_1 = BlackValidator('RUBY')
# black_val_1.validate(serving)

# black_val_2 = BlackValidator('AMELIA')
# black_val_2.validate(serving)

# black_val_3 = BlackValidator('RABBACK')
# black_val_3.validate(serving)

class LatteValidator(ValidaorBase):
    def __init__(self, name):
        self.name = name
        
    def validate(self, serving):
        if self.name in serving and serving[self.name] == CoffeeTypes.LATTE:
            raise ValidationError(f"{self.name} hate to have latte")

# late_val_1 = LatteValidator("JESSICA")
# late_val_1.validate(serving)

# late_val_2 = LatteValidator("CHLOE")
# late_val_2.validate(serving)

# late_val_3 = LatteValidator("SNORT")
# late_val_3.validate(serving)