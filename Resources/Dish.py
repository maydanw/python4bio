class Dish:
    def __init__(self,cells=None, thresholds=None):
        if cells is None:
            cells = []
        self.cells = cells
        if thresholds is None:
            thresholds=[100, 300]
        self.thresholds = thresholds
        
    def __str__(self):
        s = f"Thresholds: {self.thresholds}\n"
        for c in self.cells:
            s = s + "Cell -> " + str(c) + "\n"
        return s 

    def __repr__(self):
        return self.__str__()    
    
    def calc_mean(self):
        # enter your function here
        return len(self.cells) 
    
    def add_cell(self, dict_data):
        c = Cell(dict_data["width"], dict_data["height"], dict_data["total_intensity"])
        self.cells.append(c)