class Cell:
    def __init__(self,width, height, total_intensity):
        self.width = width
        self.height = height
        self.total_intensity = total_intensity
    
    def __repr__(self):
        return self.__str__()
        
    def __str__(self):
        return f"width: {self.width}, height: {self.height}, total intensity: {self.total_intensity}"
    
    