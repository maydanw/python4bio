# Python4Bio

## installations
* [Python Anaconda 3.6](https://www.anaconda.com/download/)
* [Git](https://git-scm.com/)
* Recommended: a Git Client like [TortoiseGit](https://tortoisegit.org/download/) 
* Optional: [PyCharm](https://www.jetbrains.com/pycharm/)


## Download the code:
1. Press winkey+R and write `cmd`
1. Navigate to where the code should be using `cd`
1. run `git clone https://gitlab.com/maydanw/python4bio`
1. `cd python4bio`
1. `jupyter notebook`